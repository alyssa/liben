/*
 * liben - Exposure Notifications implementation for GNU/Linux
 *
 * Copyright (C) 2020 Alyssa Rosenzweig
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>
#include <string.h>
#include <openssl/evp.h>
#include <openssl/kdf.h>
#include <openssl/rand.h>
#include "en.h"

/* ENIntervalNumber = timestamp / (60 * 10) */

uint32_t
generate_enin(time_t timestamp)
{
	uint64_t val = timestamp / (60 * 10);
	return (uint32_t) val;
}

static void
test_generate_enin()
{
	/* Test also beyond 32-bit */
	assert(generate_enin(12345678) == 20576);
	assert(generate_enin(1ull << 35ull) == 57266230);
}

/* TEKRollingPeriod of 24 hours */

#define TEK_ROLLING_PERIOD (144)

/* Temporary Exposure Key: i = ENIN rounded down to TEKRollingPeirod */

uint32_t
generate_tek_i(uint32_t enin)
{
	return (enin / TEK_ROLLING_PERIOD) * TEK_ROLLING_PERIOD;
}

static void
test_generate_tek_i()
{
	assert(generate_tek_i(TEK_ROLLING_PERIOD * 12345) == (TEK_ROLLING_PERIOD * 12345));
	assert(generate_tek_i((TEK_ROLLING_PERIOD * 12345) + 17) == (TEK_ROLLING_PERIOD * 12345));
	assert(generate_tek_i((TEK_ROLLING_PERIOD * 12345) + (TEK_ROLLING_PERIOD - 1)) == (TEK_ROLLING_PERIOD * 12345));
	assert(generate_tek_i((TEK_ROLLING_PERIOD * 12345) + TEK_ROLLING_PERIOD) == (TEK_ROLLING_PERIOD * 12346));
}

/* Temporary Exposure Key: "tek_i = CRNG(16)" */

void
generate_tek(uint8_t *out)
{
	RAND_bytes(out, TEK_SIZE);
}

static void
test_generate_tek(void)
{
	uint8_t key_1[16], key_2[16];
	
	generate_tek(key_1);
	generate_tek(key_2);

	/* Keys should differ. Test has 1 in 2^128 chance in failing if
	 * truly random */
	assert(memcmp(key_1, key_2, TEK_SIZE) != 0);
}

/* HKDF with HMAC-SHA256, no salts, ikm length = 16, output length = 16 */

static void
hkdf(uint8_t *output, const uint8_t *input_key, const unsigned char *info, size_t info_len)
{
	EVP_PKEY_CTX *pctx = EVP_PKEY_CTX_new_id(EVP_PKEY_HKDF, NULL);
	size_t outlen = 16;
	int ret = 0;

	ret = EVP_PKEY_derive_init(pctx);
	assert(ret == 1);

	ret = EVP_PKEY_CTX_set_hkdf_md(pctx, EVP_sha256());
	assert(ret == 1);

	ret = EVP_PKEY_CTX_set1_hkdf_key(pctx, input_key, 16);
	assert(ret == 1);

	ret = EVP_PKEY_CTX_add1_hkdf_info(pctx, info, info_len);
	assert(ret == 1);

	ret = EVP_PKEY_derive(pctx, output, &outlen);
	assert(ret == 1);
}

static void
hkdf_info_str(uint8_t *out, const uint8_t *input_key, const char *info)
{
	size_t len = strlen(info);
	hkdf(out, input_key, (const unsigned char *) info, len);
}

/* Rolling Proximity Identifier Key:
 *
 *	rpik_i = HKDF(tek_i, NULL, UTF-8("EN-RPIK"), 16)
 *
 */

void
generate_rpik(uint8_t *out, const uint8_t *tek)
{
	hkdf_info_str(out, tek, "EN-RPIK");
}

static void
test_generate_rpik(void)
{
	uint8_t tek[16] = { 0x75, 0xc7, 0x34, 0xc6, 0xdd, 0x1a, 0x78, 0x2d,
		0xe7, 0xa9, 0x65, 0xda, 0x5e, 0xb9, 0x31, 0x25 }; 

	uint8_t expected[16] = { 0x18, 0x5a, 0xd9, 0x1d, 0xb6, 0x9e, 0xc7,
		0xdd, 0x04, 0x89, 0x60, 0xf1, 0xf3, 0xba, 0x61, 0x75 };

	uint8_t out[16] = { 0 };

	generate_rpik(out, tek);
	assert(memcmp(out, expected, 16) == 0);
}

/* Rolling Proximity Identifier:
 *
 * 	rpi = AES128(RPIK, PaddedData)
 * 	PaddedData[0....5] = UTF8("EN-RPI")
 * 	PaddedData[6...11] = 0...0
 * 	PaddedData[12..15] = ENIN (little-endian)
 */

static void
generate_padded_data(uint8_t *out, uint32_t enin)
{
	uint8_t val[16] = {
		'E', 'N', '-', 'R',
	       	'P', 'I', 0x0, 0x0,
		0x0, 0x0, 0x0, 0x0,

		(enin >> 0) & 0xFF,
		(enin >> 8) & 0xFF,
		(enin >> 16) & 0xFF,
		(enin >> 24) & 0xFF,
	};

	memcpy(out, val, 16);
}

static void
test_generate_padded_data(void)
{
	uint8_t arr[16];

	/* Fill with garbage to check zero-initialization */
	memset(arr, 0xCA, 16);
	generate_padded_data(arr, 0xCAFEBABE);

	uint8_t expected[] = {
		'E', 'N', '-', 'R',
	       	'P', 'I', 0x0, 0x0,
		0x0, 0x0, 0x0, 0x0,
		0xBE, 0xBA, 0xFE, 0xCA
	};

	/* should match */
	assert(memcmp(expected, arr, 16) == 0);
}

/* Out should be 16-bytes */

void
generate_rpi(uint8_t *out, const uint8_t *rpik, uint32_t enin)
{
	uint8_t temp[32];
	uint8_t padded[16];
	signed len_1 = 0, len_2 = 0;
	int ret = 0;

	generate_padded_data(padded, enin);
	EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new();
	assert(ctx != NULL);

	ret = EVP_CIPHER_CTX_set_padding(ctx, 0);
	assert(ret == 1);

	ret = EVP_EncryptInit_ex(ctx, EVP_aes_128_ecb(), NULL, rpik, NULL /* iv */);
	assert(ret == 1);

	ret = EVP_EncryptUpdate(ctx, temp, &len_1, padded, 16);
	assert(ret == 1);
	assert(len_1 == 16);

	ret = EVP_EncryptFinal_ex(ctx, temp + len_1, &len_2);
	assert(ret == 1);
	/* XXX: why is len_2 non-zero? */

	memcpy(out, temp, 16);
}

static void
test_generate_rpi()
{
	uint8_t rpik[16] = { 0x18, 0x5a, 0xd9, 0x1d, 0xb6, 0x9e, 0xc7, 0xdd,
		0x04, 0x89, 0x60, 0xf1, 0xf3, 0xba, 0x61, 0x75
	};

	uint8_t expected[4][16] = {
		{ 0x8b, 0xe6, 0xcd, 0x37, 0x1c, 0x5c, 0x89, 0x16, 0x04, 0xbf,
			0xbe, 0x49, 0xdf, 0x84, 0x50, 0x96 },
		{ 0x3c, 0x9a, 0x1d, 0xe5, 0xdd, 0x6b, 0x02, 0xaf, 0xa7, 0xfd,
			0xed, 0x7b, 0x57, 0x0b, 0x3e, 0x56 },
		{ 0x24, 0x3f, 0xfe, 0x9a, 0x3b, 0x08, 0xbd, 0xed, 0x30, 0x94,
			0xba, 0xc8, 0x63, 0x0b, 0xb8, 0xad },
		{ 0xdf, 0xc3, 0xed, 0x26, 0x5e, 0x97, 0xd0, 0xea, 0xbb, 0x63,
			0x0e, 0x16, 0x8b, 0x42, 0x14, 0xed }
	};

	uint8_t out[16] = { 0 };
	uint32_t enin = 2642976;

	for (unsigned i = 0; i < 4; ++i) {
		generate_rpi(out, rpik, enin + i);
		assert(memcmp(expected[i], out, 16) == 0);
	}
}

/* Associated Encrypted Metadata Key
 *
 *	aemk_i = HKDF(tek_i, NULL, UTF-8("EN-AEMK"), 16)
 */

void
generate_aemk(uint8_t *out, const uint8_t *tek)
{
	hkdf_info_str(out, tek, "EN-AEMK");
}

static void
test_generate_aemk(void)
{
	uint8_t tek[16] = { 0x75, 0xc7, 0x34, 0xc6, 0xdd, 0x1a, 0x78, 0x2d,
		0xe7, 0xa9, 0x65, 0xda, 0x5e, 0xb9, 0x31, 0x25 }; 

	uint8_t expected[16] = { 0xd5, 0x7c, 0x46, 0xaf, 0x7a, 0x1d, 0x83,
		0x96, 0x5b, 0x9b, 0xed, 0x8b, 0xd1, 0x52, 0x93, 0x6a };

	uint8_t out[16] = { 0 };

	generate_aemk(out, tek);
	assert(memcmp(out, expected, 16) == 0);
}

/* Associated Encrypted Metadata
 *
 * 	AES128_CTR(AEMK, RPI, Metadata)
 */

uint32_t
generate_aem(const uint8_t *aemk, const uint8_t *rpi, uint32_t metadata)
{
	uint8_t *in = (uint8_t *) &metadata;
	uint8_t out[4];
	int ret = 0;
	signed len_1 = 0, len_2 = 0;

	EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new();
	assert(ctx != NULL);

	ret = EVP_EncryptInit_ex(ctx, EVP_aes_128_ctr(), NULL, aemk, rpi);
	assert(ret == 1);

	ret = EVP_EncryptUpdate(ctx, out, &len_1, in, 4);
	assert(ret == 1);
	assert(len_1 == 4);

	ret = EVP_EncryptFinal_ex(ctx, out + len_1, &len_2);
	assert(ret == 1);
	assert(len_2 == 0);

	uint32_t outw;
	memcpy(&outw, out, 4);
	return outw;
}

static void
test_generate_aem()
{
	uint8_t aemk[16] = { 0xd5, 0x7c, 0x46, 0xaf, 0x7a, 0x1d, 0x83, 0x96,
		0x5b, 0x9b, 0xed, 0x8b, 0xd1, 0x52, 0x93, 0x6a };

	uint8_t rpi[4][16] = {
		{ 0x8b, 0xe6, 0xcd, 0x37, 0x1c, 0x5c, 0x89, 0x16, 0x04, 0xbf,
			0xbe, 0x49, 0xdf, 0x84, 0x50, 0x96 },
		{ 0x3c, 0x9a, 0x1d, 0xe5, 0xdd, 0x6b, 0x02, 0xaf, 0xa7, 0xfd,
			0xed, 0x7b, 0x57, 0x0b, 0x3e, 0x56 },
		{ 0x24, 0x3f, 0xfe, 0x9a, 0x3b, 0x08, 0xbd, 0xed, 0x30, 0x94,
			0xba, 0xc8, 0x63, 0x0b, 0xb8, 0xad },
		{ 0xdf, 0xc3, 0xed, 0x26, 0x5e, 0x97, 0xd0, 0xea, 0xbb, 0x63,
			0x0e, 0x16, 0x8b, 0x42, 0x14, 0xed }
	};

	uint32_t expected[4] = {
		0x74380372,
		0xB11192C2,
		0x03ADDF6A,
		0x0BF8E2F1
	};

	uint32_t metadata = 0x00000840;

	for (unsigned i = 0; i < 4; ++i) {
		uint32_t aem = generate_aem(aemk, rpi[i], metadata);
		assert(aem == expected[i]);
	}
}

uint32_t
decrypt_aem(const uint8_t *aemk, const uint8_t *rpi, uint32_t aem)
{
	uint8_t *in = (uint8_t *) &aem;
	uint8_t out[4];
	signed len_1, len_2;

	EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new();
	EVP_DecryptInit_ex(ctx, EVP_aes_128_ctr(), NULL, aemk, rpi);
	EVP_DecryptUpdate(ctx, out, &len_1, in, 4);
	EVP_DecryptFinal_ex(ctx, out + len_1, &len_2);
	assert(len_1 == 4);

	uint32_t outw;
	memcpy(&outw, out, 4);
	return outw;
}

static void
test_decrypt_aem()
{
	uint8_t aemk[16] = { 0xd5, 0x7c, 0x46, 0xaf, 0x7a, 0x1d, 0x83, 0x96,
		0x5b, 0x9b, 0xed, 0x8b, 0xd1, 0x52, 0x93, 0x6a };

	uint8_t rpi[16] = { 0x8b, 0xe6, 0xcd, 0x37, 0x1c, 0x5c, 0x89, 0x16,
		0x04, 0xbf, 0xbe, 0x49, 0xdf, 0x84, 0x50, 0x96 };

	uint32_t aem = 0x74380372;
	uint32_t expected = 0x840;

	assert(decrypt_aem(aemk, rpi, aem) == expected);
}

void
test_crypto()
{
	test_generate_enin();
	test_generate_tek_i();
	test_generate_tek();
	test_generate_padded_data();
	test_generate_rpik();
	test_generate_rpi();
	test_generate_aemk();
	test_generate_aem();
	test_decrypt_aem();
}
