/*
 * liben - Exposure Notifications implementation for GNU/Linux
 *
 * Copyright (C) 2020 Alyssa Rosenzweig
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifndef __EN_H
#define __EN_H

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sqlite3.h>

sqlite3 * init_db(const char *path);
void save_received(sqlite3 *db, const uint8_t *beacon, time_t now);

/* Constants for exposure notifications packets per the Bluetooth Specification
 * v1.2.2 */
#define TEK_SIZE (16)
#define KEY_SIZE (TEK_SIZE) /* same for RPIK and AEMK */
#define RPI_SIZE (16)

#define EN_FLAG_SIZE (3)
#define EN_HDR_SIZE (4)
#define EN_DATA_HDR_SIZE (4)
#define EN_RPI_SIZE (16)
#define EN_META_SIZE (4)
#define EN_DATA_SIZE (EN_DATA_HDR_SIZE + EN_RPI_SIZE + EN_META_SIZE)
#define EN_PACKET_SIZE (EN_FLAG_SIZE + EN_HDR_SIZE + EN_DATA_SIZE)

/* Fixed template to begin the packet */
#define EN_TMPL_SIZE (EN_FLAG_SIZE + EN_HDR_SIZE + EN_DATA_HDR_SIZE)
static const uint8_t en_template[EN_TMPL_SIZE] = {
	0x02, 0x01, 0x1A, /* Flags */
	0x03, 0x03, 0x6F, 0xFD, /* Service Header */
	0x17, 0x16, 0x6F, 0xFD, /* Service data header */
};

struct en_body {
	uint8_t rpi[EN_RPI_SIZE]; /* Rolling proximity identifier */
	uint32_t aem;
} __attribute__((packed));

/* Private key material. Rotate once per TEKRollingPeriod */

struct tek_context {
	uint8_t tek[KEY_SIZE];
	uint8_t rpik[KEY_SIZE];
	uint8_t aemk[KEY_SIZE];
};

sqlite3 * init_db(const char *path);
void close_db(sqlite3 *db);
void create_beacon(uint8_t *out, struct tek_context *ctx, int8_t tx_power, time_t now);
void get_tek(struct tek_context *out, sqlite3 *db);

uint32_t generate_enin(time_t timestamp);
uint32_t generate_tek_i(uint32_t enin);
void generate_tek(uint8_t *out);
void generate_rpik(uint8_t *out, const uint8_t *tek);
void generate_rpi(uint8_t *out, const uint8_t *rpik, uint32_t enin);
void generate_aemk(uint8_t *out, const uint8_t *tek);
uint32_t generate_aem(const uint8_t *aemk, const uint8_t *rpi, uint32_t metadata);
uint32_t decrypt_aem(const uint8_t *aemk, const uint8_t *rpi, uint32_t aem);

void test_crypto();
void test_match_positive();

#endif
