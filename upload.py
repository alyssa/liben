"""
liben - Exposure Notifications implementation for GNU/Linux

Copyright (C) 2020 Alyssa Rosenzweig

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation; either version 2 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc., 51
Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
"""

import sys
import time
import nacl.utils
import nacl.public
import covidshield_pb2
import urllib.request
import sqlite3

ENDPOINT = 'http://retrieval.covid-keyserver.fake:8000'
DATABASE = 'scan.db'

def post_pb(url, obj):
    req =  urllib.request.Request(url, data = obj.SerializeToString())
    req.add_header('Content-Type', 'application/x-protobuf')
    return urllib.request.urlopen(req).read()

if len(sys.argv) != 2:
    print("usage: claim.py [one time code]")
    sys.exit(1)

one_time_code = sys.argv[1]

# Fetch the past 14 days
min_enin = (int(time.time()) - (14 * 24 * 3600)) // 600

# Fetch our keys from the database
keys = []

conn = sqlite3.connect(DATABASE)
c = conn.cursor()
for row in c.execute('SELECT min_enin, tek FROM daily_keys WHERE min_enin > ?', (min_enin,)):
    # TODO: risk estimation?
    keys.append((row[1], 4, row[0]))

# Generate one-time-use application key
privkey = nacl.public.PrivateKey.generate()

# Claim the key
kcr = covidshield_pb2.KeyClaimRequest()
kcr.one_time_code = one_time_code
kcr.app_public_key = bytes(privkey.public_key)

rest = post_pb(ENDPOINT + '/claim-key', kcr)
resp = covidshield_pb2.KeyClaimResponse()
resp.ParseFromString(rest)

if resp.error != covidshield_pb2.KeyClaimResponse.NONE:
    print("Key claim fail from uploading server:")
    print(resp)
    sys.exit(1)

# Now we have the keys exchanged, so do the upload
upload = covidshield_pb2.Upload()
upload.timestamp.GetCurrentTime()

for (data, risk, rsin) in keys:
    key = upload.keys.add()
    key.key_data = data
    key.transmission_risk_level = risk
    key.rolling_start_interval_number = rsin

eur = covidshield_pb2.EncryptedUploadRequest()
eur.server_public_key = resp.server_public_key
eur.app_public_key = kcr.app_public_key

server_pub_key = nacl.public.PublicKey(eur.server_public_key)
box = nacl.public.Box(privkey, server_pub_key)
msg = box.encrypt(upload.SerializeToString())
eur.payload = msg.ciphertext
eur.nonce = msg.nonce
rest = post_pb(ENDPOINT + '/upload', eur)

resp = covidshield_pb2.EncryptedUploadResponse()
resp.ParseFromString(rest)
print(resp)
