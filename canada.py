"""
liben - Exposure Notifications implementation for GNU/Linux

Copyright (C) 2020 Alyssa Rosenzweig

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation; either version 2 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc., 51
Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
"""

import datetime
import hashlib, hmac
import urllib.request
import zipfile
import covidshield_pb2
import io
from ctypes import CDLL, create_string_buffer, c_char_p

# Configuration
DATABASE = "scan.db"
RETRIEVE_URL="http://retrieval.covid-keyserver.fake:8001"
HMAC_KEY=bytearray.fromhex("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
REGION = "302" # Canada

clib = CDLL("./liben.so")

# Specification: https://github.com/cds-snc/covid-alert-server/tree/master/proto

def retrieval_url(region, dt, retrieve_url, hmac_key, dt_now):
    date_number = int(dt.timestamp()) // 86400
    current_hour = int(dt_now.timestamp()) // 3600

    hmac_msg = "{}:{}:{}".format(region, date_number, current_hour)
    hm = hmac.digest(hmac_key, bytes(hmac_msg, 'ascii'), "sha256").hex()

    return "{}/retrieve/{}/{}/{}".format(retrieve_url, region, date_number, hm)

def test_retrieval_url():
    MOCK_RETRIEVE_URL="https://retrieval.covidshield.app"
    MOCK_HMAC_KEY=bytearray.fromhex("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
    mock_now = datetime.datetime(2020, 5, 21, 11, 0)
    mock_dt = datetime.datetime(2020, 6, 18, 0, 0)
    mock_url = retrieval_url(REGION, mock_dt, MOCK_RETRIEVE_URL, MOCK_HMAC_KEY, mock_now)
    expected = 'https://retrieval.covidshield.app/retrieve/302/18431/dbcc782c370baba8ad974a247ae75f643901b8fa2fac9a0d822bce7c1b8795ef'
    assert(mock_url == expected)

def parse_zip(zipf):
    with zipf.open('export.bin', 'r') as f:
        export_bin = f.read()

        if len(export_bin) < 16:
            return None

        if export_bin[0:16] != b'EK Export v1    ':
            return None

        rest = export_bin[16:]

        teke = covidshield_pb2.TemporaryExposureKeyExport()
        teke.ParseFromString(rest)
        return teke

        for key in teke.keys:
            print(key)

def retrieve(dt):
    url = retrieval_url(REGION, dt, RETRIEVE_URL, HMAC_KEY, datetime.datetime.now())

    req =  urllib.request.Request(url)
    zipb = urllib.request.urlopen(req).read()

    with zipfile.ZipFile(io.BytesIO(zipb)) as zipf:
        return parse_zip(zipf)

test_retrieval_url()
teke = retrieve(datetime.datetime(year=2020, month=9, day=7))

if teke is None:
    print("parsing error")
    sys.exit(1)

db = clib.init_db(c_char_p(DATABASE.encode('ascii')))
matches = []

for key in teke.keys:
    if key.rolling_period != 144:
        print("warn: unexpected rolling period")
        print(key)

    dx_key = create_string_buffer(key.key_data)
    matched = clib.match_positive(db, dx_key, key.rolling_start_interval_number)

    if matched:
        matches.append(key)

if len(matches) > 0:
    print("!!!!!!!!!!")
    print("You may have been exposed to COVID-19.")
    print("Please contact your provincial health authority for directions.")
    print("Matching keys:")

    for match in matches:
        print(match)

    print("!!!!!!!!!!")
else:
    print("No known exposures (all clear).")

clib.close_db(db)
