all: en-daemon covidshield_pb2.py

en-daemon: liben.so daemon.c
	gcc daemon.c -L. -len -lbluetooth -lsqlite3 -lcrypto -o en-daemon

liben.so: crypto.c manager.c
	gcc crypto.c manager.c -lsqlite3 -lcrypto -shared -o liben.so

covidshield_pb2.py: covidshield.proto
	protoc --python_out=. covidshield.proto

tests: tests.c liben.so
	gcc tests.c -L. -len -o tests

test: tests
	LD_LIBRARY_PATH=. ./tests

clean:
	rm covidshield_pb2.py liben.so en-daemon
