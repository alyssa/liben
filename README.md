# liben

Experimental exposure notifications implementation for GNU/Linux.

***EXPERIMENTAL PRE-ALPHA QUALITY SOFTWARE.**

Please put on your mask before entering this repository.

----------------------------------------------------------

Build with make. Depends on:

* BlueZ
* OpenSSL
* SQLite3
* Python3
* Python3-NaCl
* Python3-Protobufs
* Protoc

Builds `en-daemon`, which should always run as a daemon to scan and advertise,
requiring Bluetooth permissions. Also provides `canada.py` and `upload.py` to
retrieve and upload keys from a sandbox of the Canadian key server (sandbox
currently hardcoded).

[Write-up](https://rosenzweig.io/blog/fun-and-games-with-exposure-notifications.html)
