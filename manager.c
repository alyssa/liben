/*
 * liben - Exposure Notifications implementation for GNU/Linux
 *
 * Copyright (C) 2020 Alyssa Rosenzweig
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>
#include <time.h>
#include <sqlite3.h>
#include "en.h"

#define METADATA_VERSION (0x40)

static uint32_t generate_metadata(int8_t tx_power)
{
	uint8_t tx_packed = 0;
	memcpy(&tx_packed, &tx_power, 1);

	return METADATA_VERSION | (((uint32_t) tx_packed) << 8);
}

/* Public encrypted payloads. Regenerate when the Bluetooth address changes
 * "about every 15 minutes" */

static void
create_rpi_aem(struct en_body *out, struct tek_context *ctx, int8_t tx_power, time_t now)
{
	uint32_t enin = generate_enin(now);

	uint32_t metadata = generate_metadata(tx_power);
	generate_rpi(out->rpi, ctx->rpik, enin);
	out->aem = generate_aem(ctx->aemk, out->rpi, metadata);
}

void
create_beacon(uint8_t *out, struct tek_context *ctx, int8_t tx_power, time_t now)
{
	struct en_body en;
	create_rpi_aem(&en, ctx, tx_power, now);

	memcpy(out, en_template, EN_TMPL_SIZE);
	memcpy(out + EN_TMPL_SIZE, &en, sizeof(en));
}

/* Database management */

sqlite3 *
init_db(const char *path)
{
	sqlite3 *db = NULL;
	int ret = 0;

	ret = sqlite3_open(path, &db);
	assert(ret == 0);

	/* Create default tables for received packets as well as our private keys */
	ret = sqlite3_exec(db, "CREATE TABLE IF NOT EXISTS received (enin INTEGER, rpi BLOB, aem INTEGER, UNIQUE(enin, rpi, aem));", NULL, NULL, NULL);
	assert(ret == 0);

	ret = sqlite3_exec(db, "CREATE TABLE IF NOT EXISTS daily_keys (min_enin INTEGER, tek BLOB, UNIQUE(min_enin, tek));", NULL, NULL, NULL);
	assert(ret == 0);
	return db;
}

void
close_db(sqlite3 *db)
{
	sqlite3_close(db);
}

/* TEKs rotate once daily, so see if we have a key from today. If not, generate
 * one and update the database. */

void
get_tek(struct tek_context *out, sqlite3 *db)
{
	int ret;
	sqlite3_stmt *tek_select, *tek_insert;

	ret = sqlite3_prepare_v2(db, "SELECT tek FROM daily_keys WHERE (min_enin > ?) ORDER BY min_enin DESC;", -1, &tek_select, NULL);
	assert(ret == SQLITE_OK);

	ret = sqlite3_prepare_v2(db, "INSERT INTO daily_keys VALUES (?, ?);", -1, &tek_insert, NULL);
	if (ret != SQLITE_OK) {
		perror("Insertion failure - probable TEK mismatch");
		exit(1);
	}

	time_t now = time(NULL);
	uint32_t current_enin = generate_enin(now);

	ret = sqlite3_reset(tek_select);
	assert(ret == SQLITE_OK);

	uint32_t min_enin = current_enin - 144; /* 1-day rolling */
	ret = sqlite3_bind_int(tek_select, 1, min_enin);
	assert(ret == SQLITE_OK);

	ret = sqlite3_step(tek_select);
	assert(ret == SQLITE_ROW || ret == SQLITE_DONE);

	if (ret == SQLITE_ROW) {
		/* Found a TEK, extract it */
		const void *blob = sqlite3_column_blob(tek_select, 0);
		assert(blob != NULL);
		assert(sqlite3_column_bytes(tek_select, 0) == KEY_SIZE);

		/* Generate the context */
		memcpy(out->tek, blob, KEY_SIZE);
	} else {
		/* Generate a new one and store */
		generate_tek(out->tek);

		ret = sqlite3_reset(tek_insert);
		assert(ret == SQLITE_OK);

		ret = sqlite3_bind_int(tek_insert, 1, current_enin);
		assert(ret == SQLITE_OK);

		ret = sqlite3_bind_blob(tek_insert, 2, out->tek, KEY_SIZE, SQLITE_TRANSIENT);
		assert(ret == SQLITE_OK);

		ret = sqlite3_step(tek_insert);
		assert(ret == SQLITE_DONE);
	}

	generate_rpik(out->rpik, out->tek);
	generate_aemk(out->aemk, out->tek);

	sqlite3_finalize(tek_select);
	sqlite3_finalize(tek_insert);
}

/* Insert a row from scanning. Takes current time so the function is pure to
 * enable unit testing. Normal users should pass time(NULL) */

void
save_received(sqlite3 *db, const uint8_t *beacon, time_t now)
{
	int ret = 0;
	sqlite3_stmt *recv;
	
	/* Extract values */
	const struct en_body *rx = (const struct en_body *) (beacon + EN_TMPL_SIZE);

	ret = sqlite3_prepare_v2(db, "INSERT OR IGNORE INTO received VALUES (?, ?, ?);", -1, &recv, NULL);
	assert(ret == SQLITE_OK);

	uint32_t enin = generate_enin(now);
	ret = sqlite3_bind_int(recv, 1, enin);
	assert(ret == SQLITE_OK);

	ret = sqlite3_bind_blob(recv, 2, rx->rpi, KEY_SIZE, SQLITE_TRANSIENT);
	assert(ret == SQLITE_OK);

	ret = sqlite3_bind_int(recv, 3, rx->aem);
	assert(ret == SQLITE_OK);

	ret = sqlite3_step(recv);
	assert(ret == SQLITE_DONE);

	sqlite3_finalize(recv);
}

/* Given a diagnosis key, checks if we have been in contact */

bool
match_positive(sqlite3 *db, const uint8_t *dx_key, uint32_t tek_i)
{
	sqlite3_stmt *select;
	int ret = 0;

	ret = sqlite3_prepare_v2(db, "SELECT aem FROM received WHERE (rpi = ? AND enin > ? AND enin < ?);", -1, &select, NULL);
	assert(ret == SQLITE_OK);

	/* Generate each of the 144 keys */
	for (unsigned i = 0; i < 144; ++i) {
		/* The spec says "A +/- 2-hour tolerance window is allowed" */
		uint32_t expected_enin = tek_i + i;
		uint32_t min_enin = expected_enin - (6 * 2);
		uint32_t max_enin = expected_enin + (6 * 2);

		/* Generate the keys */
		uint8_t rpik[KEY_SIZE], rpi[KEY_SIZE];
		generate_rpik(rpik, dx_key);
		generate_rpi(rpi, rpik, expected_enin);

		/* Query the database */
		ret = sqlite3_reset(select);
		assert(ret == SQLITE_OK);

		ret = sqlite3_bind_blob(select, 1, rpi, KEY_SIZE, SQLITE_TRANSIENT);
		assert(ret == SQLITE_OK);

		ret = sqlite3_bind_int(select, 2, min_enin);
		assert(ret == SQLITE_OK);

		ret = sqlite3_bind_int(select, 3, max_enin);
		assert(ret == SQLITE_OK);

		ret = sqlite3_step(select);
		assert(ret == SQLITE_ROW || ret == SQLITE_DONE);

		/* No match */
		if (ret == SQLITE_DONE)
			continue;

		/* Match. In the future we might decrypt the AEM at this point */
		sqlite3_finalize(select);
		return true;
	}

	sqlite3_finalize(select);
	return false;
}

/* Tests positivity matching with some in-memory databases */
static void
test_match_positive_impl(bool should_include, signed drift)
{
	sqlite3 *db = init_db(":memory:");

	/* Matches within 2 hours */
	bool should_match = should_include && abs(drift) < (3600 * 2);

	/* Reference time */
	time_t doomsday = 1584158400;

	/* A hypothetical diagnosis key */
	uint8_t dx_key[16] = { 0xCA, 0xFE, 0xBA, 0xBE, 0xCA, 0xFE, 0xBA, 0xBE,
		0xCA, 0xFE, 0xBA, 0xBE, 0xCA, 0xFE, 0xBA, 0xBE };
	time_t dx_time = doomsday + (40 * 3600);

	/* Pretend we've seen some beacons. Just make something up */

	uint8_t beacon[EN_PACKET_SIZE];
	memcpy(beacon, en_template, EN_TMPL_SIZE);

	for (unsigned j = 0; j < 100; ++j) {
		for (unsigned i = EN_TMPL_SIZE; i < EN_PACKET_SIZE; ++i)
			beacon[i] = (i * j) & 0xFF;

		save_received(db, beacon, doomsday + j * 3600);

		/* Mix in the diagnosis key */
		if (should_include && j == 50) {
			uint8_t positive[EN_PACKET_SIZE];
			struct tek_context ctx;
			memcpy(&ctx.tek, dx_key, KEY_SIZE);
			generate_rpik(ctx.rpik, ctx.tek);
			generate_aemk(ctx.aemk, ctx.tek);
			create_beacon(positive, &ctx, 10, dx_time);
			save_received(db, positive, dx_time + drift);
		}
	}

	/* Try matching */
	bool matched = match_positive(db, dx_key, generate_tek_i(generate_enin(dx_time)));
	assert(should_match == matched);
}

void
test_match_positive()
{
	for (unsigned incl = 0; incl < 2; ++incl) {
		for (signed drift = -(3600 * 3); drift <= (3600 * 3); drift += 1800)
			test_match_positive_impl(incl, drift);
	}
}
