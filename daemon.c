/*
 * liben - Exposure Notifications implementation for GNU/Linux
 *
 * Copyright (C) 2020 Alyssa Rosenzweig
 *
 * based in part on BlueZ - Bluetooth protocol stack for Linux
 *
 * Copyright (C) 2000-2001  Qualcomm Incorporated
 * Copyright (C) 2002-2003 Maxim Krasnyansky <maxk@qualcomm.com>
 * Copyright (C) 2002-2010  Marcel Holtmann <marcel@holtmann.org>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <time.h>
#include <fcntl.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include <signal.h>
#include <sqlite3.h>
#include "en.h"

/* Some BLE advertising routines missing from BlueZ
 * TODO: Upstream these? */

#define ADV_NONCONN_IND (0x3)
#define RANDOM_NONRESOLVABLE (0x1)
/* T = N * 0.625 ms <===> N = T / 0.265 ms */
#define MS_TO_BT_INTERVAL(ms) ((int) (((float) ms) / 0.625f))

static int hci_le_set_advertising_parameters(int dd, 
	uint16_t	min_interval,
	uint16_t	max_interval,
	uint8_t		advtype,
	uint8_t		own_bdaddr_type,
	uint8_t		direct_bdaddr_type,
	bdaddr_t	direct_bdaddr,
	uint8_t		chan_map,
	uint8_t		filter,
	int		to)

{
	struct hci_request rq;
	le_set_advertising_parameters_cp adv_cp;
	uint8_t status;

	memset(&adv_cp, 0, sizeof(adv_cp));
	adv_cp.min_interval       = min_interval;
	adv_cp.max_interval       = max_interval;
	adv_cp.advtype            = advtype;
	adv_cp.own_bdaddr_type    = own_bdaddr_type;
	adv_cp.direct_bdaddr_type = direct_bdaddr_type;
	adv_cp.direct_bdaddr      = direct_bdaddr;
	adv_cp.chan_map           = chan_map;
	adv_cp.filter             = filter;

	memset(&rq, 0, sizeof(rq));
	rq.ogf = OGF_LE_CTL;
	rq.ocf = OCF_LE_SET_ADVERTISING_PARAMETERS;
	rq.cparam = &adv_cp;
	rq.clen = LE_SET_ADVERTISING_PARAMETERS_CP_SIZE;
	rq.rparam = &status;
	rq.rlen = 1;

	if (hci_send_req(dd, &rq, to) < 0)
		return -1;

	if (status) {
		errno = EIO;
		return -1;
	}

	return 0;
}

static int
hci_le_set_advertising_data(int dd, uint8_t length, uint8_t *data, int to)
{
	struct hci_request rq;
	le_set_advertising_data_cp adv_cp;
	uint8_t status;

	if (length > sizeof(adv_cp.data)) {
		errno = EINVAL;
		return -1;
	}

	memset(&adv_cp, 0, sizeof(adv_cp));
	adv_cp.length = length;
	memcpy(adv_cp.data, data, length);

	memset(&rq, 0, sizeof(rq));
	rq.ogf = OGF_LE_CTL;
	rq.ocf = OCF_LE_SET_ADVERTISING_DATA;
	rq.cparam = &adv_cp;
	rq.clen = LE_SET_ADVERTISING_DATA_CP_SIZE;
	rq.rparam = &status;
	rq.rlen = 1;

	if (hci_send_req(dd, &rq, to) < 0)
		return -1;

	if (status) {
		errno = EIO;
		return -1;
	}

	return 0;
}

static int
hci_le_read_advertising_channel_tx_power(int dd, uint8_t *tx_level, int to)
{
	le_read_advertising_channel_tx_power_rp rp;
	struct hci_request rq;

	memset(&rq, 0, sizeof(rq));
	rq.ogf    = OGF_LE_CTL;
	rq.ocf    = OCF_LE_READ_ADVERTISING_CHANNEL_TX_POWER;
	rq.rparam = &rp;
	rq.rlen   = LE_READ_ADVERTISING_CHANNEL_TX_POWER_RP_SIZE;

	if (hci_send_req(dd, &rq, to) < 0)
		return -1;

	if (rp.status) {
		errno = EIO;
		return -1;
	}

	if (tx_level)
		*tx_level = rp.level;

	return 0;
}

/* Initializes advertise. Call once before en_advertise. Returns error code */

int
en_init_advertise(int dd, int8_t *tx_level)
{
       /* We can only set parameters if disabled */
       int ret = hci_le_set_advertise_enable(dd, 0x0, 1000);

       if (ret) {
               perror("Failed to disable adverising");
	       return ret;
       }

       /* TX levels are encrypted into the beacon, so fetch */
       ret = hci_le_read_advertising_channel_tx_power(dd, tx_level, 1000);

       if (ret) {
               perror("Failed to read adveristing tx power");
	       return ret;
       }
}


/* Enables advertising, should be disabled after a given period. BT MAC
 * addresses will be reset by the enable/disable, so it suffices to generate
 * the beacon here. Returns an error code or 0 for success. */

int
en_advertise(int dd, struct tek_context *tek, int8_t tx_level)
{
	/* Create a fresh beacon */
	uint8_t beacon[EN_PACKET_SIZE];
	create_beacon(beacon, tek, tx_level, time(NULL));

	printf("ADVERTISING ---- ");
	for (unsigned i = 0; i < EN_PACKET_SIZE; ++i)
		printf("%02X ", beacon[i]);
	printf("\n");

	bdaddr_t addr;
	memset(&addr, 0, sizeof(addr));

	/* Broadcasting Behaviour in the EN Bluetooth spec */
	int ret = hci_le_set_advertising_parameters(dd, 
		MS_TO_BT_INTERVAL(200), /* 200-270ms recommended */
		MS_TO_BT_INTERVAL(270),
		ADV_NONCONN_IND,
		RANDOM_NONRESOLVABLE,
		0x00, /* Peer identity type */
		addr, /* Just leave zeroed */
		0x07, /* Default -- all 3 channels */
		0x00, /* Default filter policy */
		1000);

	if (ret) {
		perror("Failed to set adverising parameters");
		return ret;
	}

	ret = hci_le_set_advertising_data(dd, sizeof(beacon), beacon, 1000);
	if (ret) {
		perror("Failed to set adverising data");
		return ret;
	}

	return hci_le_set_advertise_enable(dd, 0x1, 1000);
}

/* When quit is set, bail */
static volatile sig_atomic_t quit = 0;

static void
signal_handler(int unused)
{
	quit = 1;
}

/* Approximate timing for scan, probably room to optimize */
#define SCAN_LENGTH 15

static void
save_scan(sqlite3 *db, int dd)
{
	unsigned char buf[HCI_MAX_EVENT_SIZE], *ptr;
	struct hci_filter nf, of;
	socklen_t olen;
	int ret, len;

	olen = sizeof(of);
	ret = getsockopt(dd, SOL_HCI, HCI_FILTER, &of, &olen);
	assert(ret >= 0);

	hci_filter_clear(&nf);
	hci_filter_set_ptype(HCI_EVENT_PKT, &nf);
	hci_filter_set_event(EVT_LE_META_EVENT, &nf);

	ret = setsockopt(dd, SOL_HCI, HCI_FILTER, &nf, sizeof(nf));
	assert(ret >= 0);

	time_t scan_start = time(NULL);

	while ((time(NULL) - scan_start) < SCAN_LENGTH && !quit) {
		evt_le_meta_event *meta;
		le_advertising_info *info;

		while ((len = read(dd, buf, sizeof(buf))) < 0) {
			if (errno == EAGAIN)
				continue;
			else
				goto done;
		}

		ptr = buf + (1 + HCI_EVENT_HDR_SIZE);
		len -= (1 + HCI_EVENT_HDR_SIZE);

		meta = (void *) ptr;

		/* Advertising */
		if (meta->subevent != 0x02)
			goto done;

		info = (le_advertising_info *) (meta->data + 1);

		/* Filter only exposure notifications packets */
		if (info->length != EN_PACKET_SIZE)
			continue;

		if (memcmp(info->data, en_template, EN_TMPL_SIZE))
			continue;

		for (unsigned i = 0; i < EN_PACKET_SIZE; ++i)
			printf("%02X ", info->data[i]);
		printf("\n");

		save_received(db, info->data, time(NULL));
	}

done:
	setsockopt(dd, SOL_HCI, HCI_FILTER, &of, sizeof(of));
}

static int
scan_ble(sqlite3 *db, int dd)
{
	int err;
	uint8_t own_type = LE_PUBLIC_ADDRESS;
	uint8_t scan_type = 0x00; /* Passive, 0x01 for Active */
	uint8_t filter_policy = 0x00; /* 0x01 for Whitelist */
	uint16_t interval = htobs(0x0010);
	uint16_t window = htobs(0x0010);
	uint8_t filter_dup = 0x01;

	err = hci_le_set_scan_parameters(dd, scan_type, interval, window,
						own_type, filter_policy, 1000);
	if (err < 0) {
	       perror("Set scan parameters failed");
	       return err;
	}

	err = hci_le_set_scan_enable(dd, 0x01, filter_dup, 1000);

	if (err < 0) {
		perror("Failed to enable scan");
		return err;
	}

	save_scan(db,dd);

	err = hci_le_set_scan_enable(dd, 0x00, filter_dup, 1000);

	if (err < 0) {
		perror("Failed to disable scan");
		return err;
	}

	return 0;
}

int main(int argc, char **argv)
{
	int dev_id = hci_get_route(NULL);
	int dd = hci_open_dev(dev_id);

	if (dd < 0) {
		perror("Failed to open Bluetooth (is Bluetooth enabled?)");
		return 1;
	}

	sqlite3 *db = init_db("scan.db");

	if (db == NULL) {
		perror("Failed to open SQLite database");
		goto cleanup_hci;
	}

	signal(SIGINT, signal_handler);
	signal(SIGTERM, signal_handler);

	int8_t tx_level = 0;

	if (en_init_advertise(dd, &tx_level))
		goto cleanup_sqlite;

	struct tek_context tek;

	/* Main loop, interleaving scanning with advertisement maintenance.
	 * Must handle interrupts gracefully so we can cleanup on Ctrl-C. We
	 * need to rotate the RPI every 10-20 minutes, and scan minimally every
	 * 5 minutes (though the spec permits this to vary to reduce power
	 * consumption). We also need to rotate the TEK daily.
	 *
	 * By initializing with zero, all actions commence on initialization.
	 */

	signed minutes_until_tek_rotate = 0;
	signed minutes_until_rpi_rotate = 0;
	signed minutes_until_scan = 0;

	while(!quit) {
		if (!quit && minutes_until_tek_rotate <= 0) {
			get_tek(&tek, db);
			minutes_until_tek_rotate = 60 * 24;
		}

		if (!quit && minutes_until_rpi_rotate <= 0) {
			/* Disable advertise if enabled */
			int ret = hci_le_set_advertise_enable(dd, 0x0, 1000);

			if (ret) {
				perror("Failed to disable adverising");
				goto cleanup_all;
			}

			ret = en_advertise(dd, &tek, tx_level);
			if (ret) {
				perror("Enable advertisement failed");
				goto cleanup_all;
			}

			/* Random between 10-20 minutes, or 10-17 minutes here */
			minutes_until_rpi_rotate = 10 + (rand() & 0x7);
		}

		if (!quit && minutes_until_scan <= 0) {
			if (scan_ble(db, dd))
				goto cleanup_all;

			minutes_until_scan = 5;
		}

		if (!quit)
			sleep(60);

		minutes_until_tek_rotate--;
		minutes_until_rpi_rotate--;
		minutes_until_scan--;
	}

	/* Cleanup - disable advertisement if enabled but proceed regardless */
cleanup_all:
	if (hci_le_set_advertise_enable(dd, 0x0, 1000))
		perror("Failed to disable adverising in cleanup");

cleanup_sqlite:
	sqlite3_close(db);

cleanup_hci:
	hci_close_dev(dd);
	return 0;
}
